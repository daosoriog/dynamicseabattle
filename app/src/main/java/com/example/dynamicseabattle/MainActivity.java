package com.example.dynamicseabattle;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.dynamicseabattle.model.BaseShip;
import com.example.dynamicseabattle.model.Game;
import com.example.dynamicseabattle.model.GameTeam;
import com.example.dynamicseabattle.model.Player;
import com.example.dynamicseabattle.model.Ship;
import com.example.dynamicseabattle.model.Team;
import com.example.dynamicseabattle.model.User;
import com.example.dynamicseabattle.ui.dashboard.DashboardFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class MainActivity extends AppCompatActivity {
    private Game game;
    private User user;
    private TextView output;
    private OkHttpClient client;
    private ServerGame sg;
    ServerSocket serverSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        this.user = new User("Pepito");
        //this.game = new Game();
        //client = new OkHttpClient();

        /*Player[] players = new Player[]{new Player(user)};
        ArrayList<BaseShip> shipst1= new ArrayList<BaseShip>();
        shipst1.add(new BaseShip(new int[]{1,2},2));
        shipst1.add( new BaseShip(new int[]{3,1}));
        Team team = new Team(players,"mi equipo");
        GameTeam gt = new GameTeam(team);
        this.game.setBs(shipst1);
        this.game.gameTeams.add(gt);
        */

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*private void start() {
        Request request = new Request.Builder().url("ws://echo.websocket.org").build();
        ServerGame listener = new ServerGame();
        //WebSocket ws = client.newWebSocket(request, listener);    client.dispatcher().executorService().shutdown();
    }
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }*/
    public void startGameServer() {
     //   String f=getLocalIpAddress();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerGame sgm = new ServerGame(getLocalIpAddress());
                    sgm.start();
                    setSg(sgm);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }

            }
        });

      //  this.sg = new ServerGame(getLocalIpAddress());
    }
  /*  public void startClientGame() {
        //   String f=getLocalIpAddress();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    DashboardFragment.ClientGame2 cl = new DashboardFragment.ClientGame2("127.0.0.1");
                    cl.play();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        //  this.sg = new ServerGame(getLocalIpAddress());

    }

   */
    public void setSg(ServerGame sg) {
        this.sg = sg;
    }

    public String getLocalIpAddress() throws UnknownHostException {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        assert wifiManager != null;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipInt = wifiInfo.getIpAddress();
        return InetAddress.getByAddress(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array()).getHostAddress();
    }

}
