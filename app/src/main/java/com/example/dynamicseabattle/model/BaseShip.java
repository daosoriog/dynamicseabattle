package com.example.dynamicseabattle.model;

public class BaseShip {
    int cantidad;
    public int[] dimension;

    public BaseShip(int[] dimension) {
        this.dimension = dimension;
        this.cantidad=1;
    }
    public BaseShip(int[] dimension, int c) {
        this.dimension = dimension;
        this.cantidad=c;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int[] getDimension() {
        return dimension;
    }
}
