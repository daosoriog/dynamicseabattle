package com.example.dynamicseabattle.model;

public class Cell {
    private String state;
    public Cell(){
        this.state="empty";
    }
    public Cell(String s){
        this.state=s;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
