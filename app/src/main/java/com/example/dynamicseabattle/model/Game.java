package com.example.dynamicseabattle.model;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import com.example.dynamicseabattle.model.Team;

public class Game {
    public ArrayList<GameTeam> gameTeams;
    String state;
    private ArrayList<BaseShip> bs;
    public int[] dimensionMap;
    Team currentTeam;
    private static boolean active=false;

    public Game() {
        gameTeams = new ArrayList<GameTeam>(2);
        gameTeams.add(new GameTeam());
        gameTeams.add(new GameTeam());



        this.state = "Iniciando";
        active=true;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<BaseShip> getBs() {
        return bs;
    }
    public void initBsDefault(){
        ArrayList<BaseShip> bs = new ArrayList<BaseShip>();
        bs.add(new BaseShip(new int[]{1,2},2));
        bs.add( new BaseShip(new int[]{3,1}));
        this.setBs(bs);
    }
    public void setBs(ArrayList<BaseShip> bs) {
        this.bs = bs;
       // this.gameTeams.get(0).setShips(bs);
    }

    public boolean hasWinner() {
        return false;
    }
    /*public synchronized void attack(int location, Team team) {
        if (team != currentTeam) {
            throw new IllegalStateException("Not your turn");
        } else if (team.opponent == null) {
            throw new IllegalStateException("You don't have an opponent yet");
        } else if (*//*board[location]*/ //location != 0 /*null*/) {
         /*   throw new IllegalStateException("Cell already occupied");
        }
        //board[location] = currentPlayer;
        currentTeam = currentTeam.opponent;
    }*/

    protected boolean isActive(){
        return this.active;
    }

    public static void setActive(boolean active) {
        Game.active = active;
    }








    /*


    public class Team2 implements Runnable {
        Team opponent;
        Socket socket;
        String color, name;
        int id;
        Scanner input;

        GameTeam gt;


        public Team(Socket socket, String color, int id, String name){
            this.socket=socket;
            this.color=color;
            this.id=id;
            this.name=name;
            this.gt = new GameTeam();
        }


        @Override
        public void run() {
            try {
                System.out.println("Entraaaaa");

                output = new PrintWriter(socket.getOutputStream(), true);
                while(isActive()){
                    switch (getState()){
                        case "Esperando Oponente":
                            output.println("WAITOPONENT");
                            break;
                        case "Organizando barcos":

                            break;
                    }
                }
                //processCommands();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (opponent != null && opponent.output != null) {
                    opponent.output.println("OTHER_PLAYER_LEFT");
                }
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }

        }



        private void processCommands() {
            while (input.hasNextLine()) {
                String command = input.nextLine();
                if (command.startsWith("QUIT")) {
                    return;
                } else if (command.startsWith("ATTACK")) {
                    processMoveCommand(Integer.parseInt(command.substring(5)));
                }
            }
        }

        private void processMoveCommand(int location) {
            try {
                attack(location, this);
                output.println("VALID_MOVE");
                opponent.output.println("OPPONENT_MOVED " + location);
                if (hasWinner()) {
                    output.println("VICTORY");
                    opponent.output.println("DEFEAT");
                }/* else if (boardFilledUp()) {
                    output.println("TIE");
                    opponent.output.println("TIE");
                }*//*
            } catch (IllegalStateException e) {
                output.println("MESSAGE " + e.getMessage());
            }
        }

    }
    */
}
