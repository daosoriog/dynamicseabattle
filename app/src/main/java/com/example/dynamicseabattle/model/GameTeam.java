package com.example.dynamicseabattle.model;

import java.util.ArrayList;

public class GameTeam {
    public Team team;
    public ArrayList<Ship> ships;
    Map map;


    public GameTeam() {
        this.team = new Team();
        this.map = new Map();
        this.ships = new ArrayList<Ship>();
    }
    public GameTeam(int[] dimension) {
        //this.team = team;

        this.map = new Map(dimension);
        this.ships = new ArrayList<Ship>();
    }


    public ArrayList<Ship> getShips() {
        return ships;
    }

    public void setShips(ArrayList<Ship> ships) {
        this.ships = ships;
    }

}
