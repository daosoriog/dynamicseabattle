package com.example.dynamicseabattle.model;

import java.util.ArrayList;

public class Map {
    int[] dimension;
    ArrayList<ArrayList<Cell>> map;

    public Map() {
        this.dimension = new int[]{6,6};
        System.out.println("dii "+this.dimension[0]);
        map= new ArrayList<ArrayList<Cell>>();
        this.initMap();

    }
    public Map(int[] dimension) {
        this.dimension = dimension;
        //map= new ArrayList<ArrayList<Cell>>();
        //initMap();
    }
    public Map(int[] dimension, ArrayList<ArrayList<Cell>> m) {
        this.dimension = dimension;
        this.map = m;
    }
    private void initMap(){
        for (int i=0;i<this.dimension[0];i++){
            ArrayList<Cell> row = new ArrayList<Cell>();
            for(int j=0;j<this.dimension[1];j++){
                row.add(new Cell());
            }
            map.add(row);
        }


    }

    public int[] getDimension() {
        System.out.println("sssssssssss");
        return dimension;
    }

    public ArrayList<ArrayList<Cell>> getMap() {
        return map;
    }

    public void setMap(ArrayList<ArrayList<Cell>> map) {
        this.map = map;
    }
}
