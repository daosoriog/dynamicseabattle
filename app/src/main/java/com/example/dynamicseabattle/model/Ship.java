package com.example.dynamicseabattle.model;

public class Ship {
    private String name, image, state;
    public int[] dimension, pos; // Valores (x,y)
    private int[][] campos;

    public Ship(String n, int[] d){
        this.name=n;
        this.dimension=d;
        this.state="active";
    }
    public Ship(String n, int[] d, String i){
        this.name=n;
        this.image=i;
        this.dimension=d;
        this.state="active";
    }

    private void initCampos(){
        this.campos= new int[dimension[0]][dimension[1]];
        for(int i=0;i<dimension[0];i++){ // Por cada x
            for(int j=0;j<dimension[1];j++){  // cada y
                this.campos[i][j]=0;
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int[] getDimension() {
        return dimension;
    }

    public int[] getPos() {
        return pos;
    }

    public void setPos(int[] pos) {
        this.pos = pos;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}








