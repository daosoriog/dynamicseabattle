package com.example.dynamicseabattle.model;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Team {
   // public Player[] players;
    private String nombre;
    private String color;
    public Team opponent;

    public Team() {

    }

   // public Team(Player[] pl){
    //    this.players=pl;
   // }
    public Team(Player[] pl, String n){
        //this.players=pl;
        this.nombre=n;
    }
   /* public Team(Player[] pl, String n, String c){
        this.players=pl;
        this.nombre=n;
        this.color=c;
    }*/

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public void setBs(ArrayList<BaseShip> bs){

    }


    /*
    Clase player runabble
     */
    public class PlayerR implements Runnable{
        Socket socket;
        PrintWriter output;
        String name;
        boolean active;
        String state, shipsToPrint;
        public PlayerR(Socket s, String name, String sp){
            this.shipsToPrint=sp;
            this.name=name;
            this.socket=s;
            this.active=true;
        }
        @Override
        public void run() {
            try{
                setup();
                while(isActive()) {

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        private void setup() throws IOException {
            //input = new Scanner(socket.getInputStream());
            output = new PrintWriter(socket.getOutputStream(), true);
            output.println("WELCOME "+getColor()+" "+ name);
            output.println("MAPS d");
            output.println("SHIPS "+shipsToPrint);
/*
            if (id == 1) {
               // currentTeam = this;
                output.println("MESSAGE Waiting for opponent to connect");
            } else {
                //opponent = currentTeam;
                //opponent.opponent = this;
                //opponent.output.println("MESSAGE Your move");
            }

 */
        }

        protected boolean isActive(){
            return this.active;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
