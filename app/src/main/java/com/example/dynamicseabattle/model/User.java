package com.example.dynamicseabattle.model;

public class User {
    private String nickName;
    public User(String n){
        this.nickName=n;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
