package com.example.dynamicseabattle.ui.dashboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.dynamicseabattle.MainActivity;
import com.example.dynamicseabattle.R;

import com.example.dynamicseabattle.model.BaseShip;
import com.example.dynamicseabattle.model.Game;
import com.example.dynamicseabattle.model.GameTeam;
import com.example.dynamicseabattle.model.Map;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private DashboardViewModel dashboardViewModel;
    private Game gameF;
    private Button bListo;
    private TextView labelT;
    private View r;
    private Map myMap, oponentMap;
    private int shipSelected; // representa el id del barco seleccionado
    private ClientGame2 cg;
    private GameTeam gt, opnonentTeam;
    private ArrayList<BaseShip> bs;

    @SuppressLint("ResourceType")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container,
                false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        this.setShipSelected(-1);
        //
        // Accion de listo del boton
        this.r=root;
        this.bListo=root.findViewById(R.id.buttonListo);
        bListo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                setStateGame("Jugando");
                printMap(myMap);
                //printMenu(myMap);

            }
        });
        this.labelT=root.findViewById(R.id.textViewDB);

        try {
            cg = new ClientGame2("127.0.0.1",8383);
            cg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Trae juego de Main
        //this.gameF = ((MainActivity) getActivity()).getGame();
        this.setStateGame("Organizar barcos");


        // Texto GameState
        final TextView textGameState = root.findViewById(R.id.textGameState);
        dashboardViewModel.getGameState().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textGameState.setText(s);
            }
        });
        //final TableLayout table = root.findViewById(R.id.tableMap);

//        printMap(root,myMap);
 //       printMenu(root,myMap);
   //     printLabels(root);
        //printMap2(root,gameF.gameTeams.get(0).map.getDimension());
       // printMap(table,new int[]{6,6});

        return root;
    }

    private void printMap(Map m){
        System.out.println("printMap");
        final TableLayout table = r.findViewById(R.id.tableMap);
        System.out.println("printMap2");
        table.removeAllViews();
        System.out.println("printMap3");
        int[] dim = m.getDimension();
        System.out.println("d "+dim[1]);
        TableRow[] rows = new TableRow[dim[1]];
        for(int i=0;i<dim[1];i++) {
            rows[i] = new TableRow(getActivity());
             for (int j = 0; j < dim[0]; j++) {
                 ImageButton b = new ImageButton(getActivity());
                 b.setId(99999+10*i+j);
                 b.setContentDescription("bt:"+j+","+i);
                 b.setOnClickListener(this);
                 // case map (i,j) Cell -> get estado
              //   b.setBackgroundColor(F174DF);
                 b.getBackground().setColorFilter(Color.parseColor("#81BEF7"), PorterDuff.Mode.DARKEN);
                 b.setImageResource(R.drawable.ic_crop_square_black_24dp);
                 rows[i].addView(b);
            }
            table.addView(rows[i]);
        }
    }
    private void printLabel(String s){
        this.labelT.setText(s);
    }
    private void printLabels(View root){
        if(gameF.getState()=="Organizar barcos" || gameF.getState()=="Jugando" ){
            this.bListo.setVisibility(View.VISIBLE);
        }

    }

    private void printMenu(Map m,String action){
        int[] dim = m.getDimension();
        TableLayout tlNewShips = r.findViewById(R.id.FrameMenu);
        tlNewShips.removeAllViews();
        System.out.println("Menu "+action);
        switch (action){
            case "Organizar barcos":

                TableRow[] rowstl = new TableRow[dim[0]];
                rowstl[0] = new TableRow(getActivity());
                for (int i=0;i<bs.size();i++) {
                    BaseShip s = bs.get(i);
                    ImageButton b = new ImageButton(getActivity());
                    b.setId(88888+10*i);
                    b.setImageResource(R.drawable.ic_directions_boat_black_24dp);

                    b.setContentDescription("ship:"+i+":"+s.dimension[0]+","+s.dimension[1]);
                    b.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {
                            //  printMap(r,gameF.gameTeams.get(0).map.getDimension());
                            //setStateGame("Jugando");


                        }
                    });;
                    b.setOnClickListener(new View.OnClickListener(){
                                             @Override
                                             public void onClick(View view) {
                                                 //  printMap(r,gameF.gameTeams.get(0).map.getDimension());
                                                 // setStateGame("Jugando");
                                                 System.out.println("dddddddddddddd");
                                                 ImageButton b = getActivity().findViewById(view.getId());
                                                 setNewShip(b);
                                             }
                    });
                    TextView tv = new TextView(getActivity());
                    tv.setText("("+s.dimension[0]+","+s.dimension[1]+")");
                    rowstl[0].addView(b);
                    rowstl[0].addView(tv);
                }
                tlNewShips.addView(rowstl[0]);

        }
    }

    private void setNewShip(ImageButton b){
        String desc = (String) b.getContentDescription();
        String tipo = desc.split(":")[0];
        this.setShipSelected(Integer.parseInt(desc.split(":")[1]));
    }

    public void setColor(String c){

    }
    public void setName(String c){
        System.out.println("Setcolorrrr "+c+"q");
        printLabel(c);
    }
    public void setBS(String sbs){
        String[] asbs= sbs.split(";");
        bs = new ArrayList<BaseShip>();
        for(int i=0;i<asbs.length;i++){
            String[] bb=asbs[i].split("x");
            String[] bdim=bb[1].split(",");
            bs.add(new BaseShip( new int[]{Integer.parseInt(bdim[0]),Integer.parseInt(bdim[1])},Integer.parseInt(bb[0])));
        }
        printMenu(myMap,"Organizar barcos");

    }
    public void setMapD(String md){
        //System.out.println("ee "+md);

        if(md.equals("d")){
            //System.out.println("xee "+md);
            myMap=new Map();

        }
        printMap(myMap);
    }

    @Override
    public void onClick(View v) {
        //View root=getContext();
        System.out.println(v.getId());
        //v.setText();
        //Button b = getActivity().findViewById(v.getId());
        //b.setText("0");
        ImageButton b = getActivity().findViewById(v.getId());
        String pos=((String) b.getContentDescription()).split(":")[1];
        int[] posSelected = new int[]{
                Integer.parseInt(pos.split(",")[0]),
                Integer.parseInt(pos.split(",")[1])
            };
        System.out.println(getShipSelected());
        if(gameF.getState()=="Organizar barcos" && getShipSelected() < gameF.getBs().size() && getShipSelected() >= 0) {
            //b.setImageResource(R.drawable.ic_directions_boat_black_24dp);
            setShipInMap(posSelected);
        }else if(gameF.getState()=="Jugando"){
            b.setImageResource(R.drawable.ic_blur_circular_black_24dp);
        }
        System.out.println(v);
        System.out.println(v.getContentDescription());
        System.out.println(v.getHeight());

    }



    private void setShipInMap(int[] pos){
        BaseShip bs = this.gameF.getBs().get((getShipSelected()));
        // set in game of MainActivuty | this.gameF.gameTeams[]
        printShip(pos, bs);




    }
    private void printShip(int[] pos, BaseShip bs){
        TableLayout table = this.r.findViewById(R.id.tableMap);
        for(int j=pos[1];j<(pos[1]+bs.dimension[1]);j++){
            TableRow row = (TableRow)table.getChildAt(j);
            for(int i=pos[0];i<(pos[0]+bs.dimension[0]);i++){
                ImageButton ib = (ImageButton) row.getVirtualChildAt(i);
                ib.setBackgroundColor(0x0174DF);// hace perder los bordes y margenes
                ib.getBackground().setColorFilter(Color.parseColor("#819FF7"), PorterDuff.Mode.DARKEN);

                ib.setImageResource(R.drawable.ic_directions_boat_black_24dp);
            }
        }



    }

    public int getShipSelected() {
        return shipSelected;
    }

    public void setShipSelected(int shipSelected) {
        this.shipSelected = shipSelected;
    }

    private void setStateGame(String s){
        //this.gameF.setState(s);
//        dashboardViewModel.setGameState(gameF.getState());
    }
    /*private void updateMainGame(){
        ((MainActivity) getActivity()).setGame(this.gameF);
    }*/
    public class ClientGame2 extends AsyncTask<Void, Void, Void> {

        // private Socket socket;
        private Scanner in;
        private PrintWriter out;
        private String serverAddress, response = "";
        int port;

        //  private final Handler handler;
        public ClientGame2(String serverAddress, int p/*, Context cotx*/) throws Exception {
            this.serverAddress = serverAddress;
            this.port = p;
        }


        public String getServerAddress() {
            return serverAddress;
        }

        /*  public void setSocket(Socket socket) throws IOException {
              this.socket = socket;
              //socket = new Socket(getServerAddress(), 8383);
              in = new Scanner(socket.getInputStream());
              out = new PrintWriter(socket.getOutputStream(), true);
          }
         */


        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Void doInBackground(Void... voids) {
            Socket socket = null;
            try {
                socket = new Socket(serverAddress, port);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                byte[] buffer = new byte[1024];

                int bytesRead;
                InputStream inputStream = socket.getInputStream();

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    String rs=byteArrayOutputStream.toString("UTF-8");
                    response += rs;
                    System.out.println("Whiles "+rs);
                  //  String[] lines = new BufferedReader(new StringReader(rs))
                        //    .lines()
                       //     .toArray(new String[]);
                    String[] rsl = rs.split("\\r?\\n");
                    for(int i=0;i<rsl.length;i++){
                        String[] rss =rsl[i].split(" ");
                        System.out.println("Whilesss "+rss[0]+".");
                        switch (rss[0]){
                            case "WELCOME":
                                setColor(rss[1]);
                                setName(rss[2]);
                                break;
                            case "MAPS":
                                setMapD(rss[1]);
                                break;

                            case "SHIPS":
                                setBS(rss[1]);
                                break;
                        }
                    }


                }

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response = "UnknownHostException: " + e.toString();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response = "IOException: " + e.toString();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return null;
            }
        }
        @Override
        protected void onPostExecute(Void result) {
            System.out.println(response);
            super.onPostExecute(result);
        }
    }

}
