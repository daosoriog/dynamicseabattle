package com.example.dynamicseabattle.ui.dashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DashboardViewModel extends ViewModel {

    private MutableLiveData<String> mText, gameState;

    public DashboardViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is dashboard fragment");
        gameState = new MutableLiveData<>();
        gameState.setValue("Iniciando partida");
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<String> getGameState() {
        return gameState;
    }

    public void setGameState(String gState) {
        this.gameState.setValue(gState);
    }
}